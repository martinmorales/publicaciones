El desarrollo de esta sonda fue realizado como parte del proyecto "Monitoreo Hidro-Ambiental de la Reserva Natural Urbana del Oeste de la Ciudad de Santa Fe", ganador del concurso Aguas Claras, financiado por la Fundación Bunge y Born. Los parámetros registrados con este dispositivo son publicados en https://monitorreservasfe.streamlit.app/


##### Fecha de actualización de este documento
_27 de marzo de 2023_

![sonda](img/tentacle_t1.png "sonda Tentacle 1.16")

_«Un conocimiento, a diferencia de una propiedad física, 
puede ser compartido por grandes grupos de personas
sin empobrecer a nadie»_

**Aaron Swartz (1986 - 2013)**


# Sonda Multiparamétrica
Este micro tutorial trata sobre el equipo construido a partir de la Sonda EZO de Atlas Scientific versión T1.16(*) en el marco del Proyecto "Monitoreo hidro-ambiental para la gestión del agua de la Reserva Natural Urbana del Oeste de la ciudad de Santa Fe". 

La sonda multiparamétrica está compuesta principalmente por una plaqueta (shield) EZO de Atlas Scientific que fue montada en un arduino uno, y que tiene la capacidad de albergar y aislar individualmente 4 circuitos con sensores que miden: pH (pH), Oxígeno disuelto (OD), Conductividad eléctrica (EC) y Potencial óxido-reducción (ORP).

## Directorios
La estructura del Proyecto de la Sonda Multiparamétrica esta compuesta por los siguientes directorios:
- `codigo/`: Código base original que lee los 4 sensores de forma continua con el protocolo I2C
- `sonido/` : Documentación oficial y otra info útil sobre el shield y sensores

## Descripción
**Hardware**
El Equipo Multiparamétrico está compuesto por el siguiente hardware:
- Arduino Uno - Placa Base
- Shield EZO de Atlas Scientific versión T1.16 - Con 4 sensores: PH, OD, EC y ORP
- Shield Datalogger con tarjeta SD
- Pantalla Oled

**Conexiones**
Las plaquetas van montadas en este orden:
El shield EZO de Atlas Scientific va ensamblada arriba del Arduino Uno. Arriba del shield EZO de Atlas Scientific se coloca el shield de Datalogger. El conexionado de la Pantalla Oled se realiza desde el shield del datalogger de la siguiente manera:
   
| OLED | Datalogger |
| ---- |----------- |
| SDA  | SDA (pin 1)|
| SCL  | SCL (pin 0)|
| VCC  | 5v         |
| GND  | GND        |

**Alimentación**
El equipo debe ser alimentado mediante cable USB conectado a una notebook o banco de baterias o por una batería externa de 9 voltios en el conector DC del Arduino UNO.

**Funcionamiento**
La sonda se enciende al ser alimentada, carga automáticamente un software que sensa pH, OD, EC y ORP y que muestra en pantalla. Este sensado se realiza cada 7 segundos. En la pantalla se puede observar la Fecha y la Hora de sensado y los valores de cada Sensor.
Por otra parte toda la información recabada se va registrando en el archivo "sonda.csv" en la tarjeta del dispositivo. Allí en cada "pasada" de lectura de los sensores se genera un registro con la fecha, hora, minutos y segundos, pH, OD, EC y ORP. El archivo "sonda.csv" es un archivo separado por comas, que puede ser leído por cualquier software de planilla de cálculo. 

## Instalación del Software para lecturas continuas
La sonda funciona con un software que integra la lectura de los sensores, la visualización de la información en la pantalla de la sonda y el guardado de los valores sensados en la tarjeta SD. El software (sketch) que debe tener cargado la sonda multiparamétrica para llevar adelante estas tareas está [disponible en este mismo repositorio](https://gitlab.com/reserva-oeste-sfe/sonda-multiparametrica/-/blob/main/src/base/sonda_sd_pantalla_ok.ino). Para hacer la carga de este sketch a la sonda debe tener el Entorno de Programación "Arduino Ide" instalado. 
_Para mas información dirijase a https://www.arduino.cc/en/software_

**Resumen para la instalación del software**
1. Conectar la sonda a su computadora
2. Abrir "Arduino Ide" 
3. Cargar el Sketch [disponible en este repositorio](https://gitlab.com/reserva-oeste-sfe/sonda-multiparametrica/-/blob/main/src/base/sonda_sd_pantalla_ok.ino).
4. Si la carga tuvo éxito, automáticamente en la pantalla se visualizará la fecha, hora y el valor de lectura de cada uno de los sensores. Estos valores se actualizan cada 7 segundos.

## Uso de la sonda 
La sonda multiparamétrica se enciende al conectar el cable USB de alimentación.
Los sensores deben ser conectados a la ficha de la placa correspondiente a cada uno de ellos. Los sensores son frágiles por lo que deben ser manipulados con cuidado.
Automáticamente en la pantalla se visualiza la fecha, hora y el valor de lectura de cada uno de los sensores. Estos valores se actualizan cada 7 segundos.

### Para realizar mediciones en muestras de agua 
1. Retirar el capuchón de los sensores de OD y EC.
2. Retirar los recipientes con solución de almacenamieto de los sensores de pH y ORP. 
3. Enjuagar los sensores con agua destilada, secarlos apoyando un papel suave (sin frotarlos).
4. Sumergir los sensores en el recipiente con la muestra de agua, agitar levemente para desprender alguna burbuja de aire que pueda haber quedado adherida y esperar a que el valor de lectura de la pantalla se haya estabilizado.
5. Retirar los sensores de la muestra y enjuagar con agua destilada.

_Para mas información sobre la manipulación de los sensores consultar (https://www.whiteboxes.ch/docs/tentacle/t1/#/quickstart)_

### Para apagar y guardar la sonda
1. Desconectar el cable de alimentación.
2. Enjuagar con abundante agua destilada todos los sensores.
3. Secar al aire o apoyando en un papel suave (sin frotarlos).
4. Volver a colocar el capuchón a los sensores de OD y EC.
5. Volver a colocar los recipientes con solución de almacenamiento de los sensores de pH y ORP.

### Almacenamiento de datos
Mientras la sonda esté encendida se va registrando fecha, hora, minutos, segundos, y los valores medidos de pH, OD, EC y ORP cada 7 segundos. Estos datos quedan almacenados en el archivo "sonda.csv" en la tarjeta del dispositivo y pueden ser descargados luego, retirando la tarjeta SD.

## Calibración
La calibración de la sonda multiparamétrica se realiza cargando el software original (sketch) de EZO de Atlas Scientific disponible en este mismo repositorio: ("src/base/setup_tentacle-calibracion.ino") en el Arduino Uno. Para hacerlo debe tener el software Arduino Ide instalado en su computadora. Si no lo tiene dirijase a "https://www.arduino.cc/en/software". 
Una vez cargado el sketch, el procedimiento para la calibración es sencillo. Se debe abrir el "Monitor serial" y desde allí se realizará la interacción con la sonda para calibrar todos los sensores.

**Resumen de Pasos para la calibración**
- Abrir el entorno de programación "Arduino Ide" en su computadora.
- Conectar la sonda a un puerto de su computadora
- Cargar el [sketch](https://gitlab.com/reserva-oeste-sfe/sonda-multiparametrica/-/blob/main/src/base/setup_tentacle-calibracion.ino)
- Abrir el "Monitor serial". Desde allí se realizará la interacción con la sonda para calibrar cada uno de los sensores.
- IMPORTANTE: finalizada la calibración debe cargar nuevamente en la sonda el software para Lecturas contínuas, explicado en [esta sección](https://gitlab.com/reserva-oeste-sfe/sonda-multiparametrica#instalaci%C3%B3n-del-software-para-lecturas-continuas)

### A. Calibración del Sensor de pH:
Se requiere contar con soluciones de calibración de pH 4.00, 7.00 y 10.00 (low, middle, high respectivamente). La calibración puede hacerse con uno, dos o tres puntos.
1. Verificar que el cable del sensor de pH se encuentre conectado a la sonda.
2. Para activar el canal dónde está conectado el sensor de pH, se debe tipear "97" y presionar "enter" la pantalla del "Monitor Serial". Si en la pantalla aparece la información del Sensor, su Tipo y la velocidad de transmisión de datos es porque el canal se activó correctamente.

**Preparar el sensor:** 
- Retirar el recipiente con solución de almacenamiento del sensor. 
- Enjuagarlo con agua destilada, secarlo apoyando un papel suave (sin frotarlos).

#### Calibración pH=4 (low)
1. Sumergir el sensor en la solución de calibración de pH 4, agitar levemente para desprender alguna burbuja de aire que pueda haber quedado adherida.
2. Tipear "r" y presionar "enter" en la pantalla del "Monitor Serial". En la pantalla se visualiza el valor de pH medido por la sonda. 
3. Repetir el paso 2 varias veces hasta que la lectura sea constante. 
4. Cuando se llegue a la estabilización del valor medido por la sonda, tipear "cal,low,4.0" y presionar "Enter". El software contestará si la acción fue exitosa o no.
5. Vuelva a leer los valores de sensado colocando "r" y presionando "Enter" para verificar que la lectura se corresponda con el valor de la solución.
6. Retirar la sonda de la solución, enjuagar con abundante agua destilada. Secar al aire o apoyando en un papel suave (sin frotar).

#### Calibración pH=7 (middle)
1. Sumergir el sensor en la solución de calibración de pH 7, agitar levemente para desprender alguna burbuja de aire que pueda haber quedado adherida.
2. Tipear "r" y presionar "enter" en la pantalla del "Monitor Serial". En la pantalla se visualiza el valor de pH medido por la sonda. 
3. Repetir el paso 2 varias veces hasta que la lectura sea constante. 
4. Cuando se llegue a la estabilización del valor medido por la sonda, tipear "cal,middle,7.0" y presionar "Enter". El software contestará si la acción fue exitosa o no.
5. Vuelva a leer los valores de sensado colocando "r" y presionando "Enter" para verificar que la lectura se corresponda con el valor de la solución.
6. Retirar la sonda de la solución, enjuagar con abundante agua destilada. Secar al aire o apoyando en un papel suave (sin frotar).

#### Calibración pH=10 (high)
1. Sumergir el sensor en la solución de calibración de pH 10, agitar levemente para desprender alguna burbuja de aire que pueda haber quedado adherida.
2. Tipear "r" y presionar "enter" en la pantalla del "Monitor Serial". En la pantalla se visualiza el valor de pH medido por la sonda. 
3. Repetir el paso 2 varias veces hasta que la lectura sea constante. 
4. Cuando se llegue a la estabilización del valor medido por la sonda, tipear "cal,high,10.0" y presionar "Enter". El software contestará si la acción fue exitosa o no.
5. Vuelva a leer los valores de sensado colocando "r" y presionando "Enter" para verificar que la lectura se corresponda con el valor de la solución.
6. Retirar la sonda de la solución, enjuagar con abundante agua destilada. Secar al aire o apoyando en un papel suave (sin frotar).

### Calibración del sensor de OD:
Para la calibración del sensor de OD se requiere una solución de calibración de 0 mg/l. Se realiza solo con 1 punto.
1. Verificar que el cable del sensor de OD se encuentre correctamente conectado a la sonda.
2. Para activar el canal dónde está conectado el sensor de OD, se debe tipear "98" y presionar "enter" en la pantalla del "Monitor Serial". Si en la pantalla aparece la información del Sensor, su Tipo y la velocidad de transmisión de datos es porque el canal se activó correctamente.
3. Retirar el capuchón del sensor de OD.
4. Enjuagarlo con agua destilada, secarlo apoyando un papel suave (sin frotarlo).
5. Sumergir el sensor en la solución de calibrado de OD 0 mg/L.
6. Tipear "r" y presionar "enter" en la pantalla del "Monitor Serial". En la pantalla se visualiza el valor de OD medido por la sonda. 
7. Repetir el paso 6 varias veces hasta que la lectura sea constante. 
8. Cuando se llegue a la estabilización del valor medido por la sonda, tipear "cal,0" y presionar "Enter". El software contestará si la acción fue exitosa o no.
5. Vuelva a leer los valores de sensado colocando "r" y presionando "Enter" para verificar que la lectura se corresponda con el valor de la solución.
6. Retirar la sonda de la solución, enjuagar con abundante agua destilada. Secar al aire o apoyando en un papel suave (sin frotar).


### Calibración del Sensor de Conductividad:
Para la calibración de este sensor se requieren soluciones de calibración de 12.880 μs/cm y 80.000 μs/cm. Puede realizarse con uno o dos puntos. 
1. Verificar que el cable del sensor de EC se encuentre correctamente conectado a la sonda.
2. Para activar el canal dónde está conectado el sensor de EC, se debe tipear "99" y presionar "enter" en la pantalla del "Monitor Serial". Si en la pantalla aparece la información del Sensor, su Tipo y la velocidad de transmisión de datos es porque el canal se activó correctamente.

**Preparar el sensor:**
- Retirar el capuchón del sensor. 
- Enjuagarlo con agua destilada, secarlo apoyando un papel suave (sin frotarlo).

